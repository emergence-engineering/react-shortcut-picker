# ShortcutPicker

`ShortcutPicker` is a React component for picking shortcuts. It's easy to use and to embed into your website.

- You dynamically picking the shortcut, thereby the users cannot set one that they would not be able to use due to the default settings of the browser.
- You can onfocus the field with `Enter` and `Escape`. `Enter` saves the modified keybinding, `Escape` cancels it and restores the previous one.
- You can delete the current shortcut with `Delete`, which set it to the empty default version.

You can try it out on our **[website][website]**.

<!-- You can try it out on our website: [<img alt="https://emergence-engineering.com" width="22px" src="readme-icons/web.png" />][website] -->

## 🚀 How to embed into your website

You are required to give the component the following parameters:

- **shortcutCmd: (string[] | undefined)** The current keybinding for this shortcut. The keybinding should be add as a string array. If there is no keybinding yet, undefined type should be given.

- **id: (string)** The unique identifier of the command.

- **handleShortcutCmd: (id: string, shortcutCmd: string[] | undefined) => void** A function which will be called with the given _id_ and with the _shortcutCmd_ chosen by the user. If the user deletes the command, the _shortcutCmd_ will be undefined.

These following parameters are optional:

- **maxCommandNumber: (number)** The maximum allowed number of command parts. Default: 4. Example: "Shift+2+3" will be counted as 3.

- **emptyCommand: (string)** This text will be displayed when no command is given or the user deletes it. Default: "Click to add shortcut.".

## 🎁 Example

The following code shows how easy-to-use the ShortcutPicker is:

```javascript
import ShortcutPicker from "react-shortcut-picker";

<StyledCommandNameContainer>
      {keyCommandToName[command]}
</StyledCommandNameContainer>
<ShortcutPicker
      shortcutCmd={convertLocStorage(command)}
      id={command}
      handleShortcutCmd={handleShortcutCmd}
/>
```

### TODO:

- [x] with `npm run lint:fix` you can run eslint, that checks your code for a lot of coding errors. You can find an explanation
      behind them at https://airbnb.io/javascript/ which is the plugin we're using.
- [x] There were a lot of errors in the code, but I've disabled them with comments ( like // eslint-disable-next-line no-plusplus )
      remove the disabling, fix the code, if you're interested in the rule then look it up, you might learn something.
- [ ] Try out storybook, make a small example
- [x] Finish up the README, write usage explanation
- [ ] Write a blog post on our blog, include the shortcut-picker in the blog post to display an interactive demo
- [ ] Clean up the code, but we can do that together
- [x] don't use global variables, use setState
- [.] Check out how cypress works

### Local development

To use this package in another project:

- `npm link` in `react-shortcut-picker` <-- here
- `npm link react-shortcut-picker` in the project you want to use it <-- spideryarn

Or

- `npm install path/to/project` in the project you want to use it

If you get hook error with react:
from https://stackoverflow.com/questions/65259040/react-antd-rollup-component-library-error-invalid-hook-call-hooks-can-onl

- first: [npm link] a spideryarn/web/node_modules/react -ban
- second: [npm link react] a react-shortcut-picker -ben

[website]: https://emergence-engineering.com
