/* eslint-disable jsx-a11y/tabindex-no-positive */
/* eslint-disable jsx-a11y/no-noninteractive-tabindex */
/* eslint-disable no-param-reassign */
import React, {
  FunctionComponent,
  useEffect,
  useState,
  KeyboardEvent,
  useCallback,
} from "react";

const DefaultControl: FunctionComponent = () => (
  <div className="shortcutItemWrapper">Control</div>
);
const DefaultShift: FunctionComponent = () => (
  <div className="shortcutItemWrapper">Shift</div>
);
const DefaultMeta: FunctionComponent = () => (
  <div className="shortcutItemWrapper">Meta</div>
);
const DefaultAlt: FunctionComponent = () => (
  <div className="shortcutItemWrapper">Alt</div>
);
const DefaultSpace: FunctionComponent = () => (
  <div className="shortcutItemWrapper">Space</div>
);
const DefaultText: FunctionComponent<{ text: string }> = ({ text }) => (
  <div className="shortcutItemWrapper">{text}</div>
);
const DefaultCharacter: FunctionComponent<{ character: string }> = ({
  character,
}) => <div className="shortcutItemWrapper">{character}</div>;

const isCommandPartsEmpty = (
  commandParts: string[],
  emptyCommand: string
): boolean => commandParts.length === 1 && commandParts[0] === emptyCommand;

/*
 * ShortcutPicker React Component
 */
const ShortcutPicker: FunctionComponent<{
  // required parameters
  shortcutCommand: string[] | undefined;
  id: string;
  handleShortcutPicker: (id: string, shortcutCmd: string[] | undefined) => void;

  // optional parameters
  maxCommandNumber?: number;
  emptyCommand?: string;
  allowedKeysArg?: string[];
  keyDisplayValues?: Record<string, string>;
}> = ({
  shortcutCommand,
  id,
  handleShortcutPicker,
  maxCommandNumber = 4,
  emptyCommand = "Click to add shortcut.",
  allowedKeysArg,
  keyDisplayValues = {
    Control: DefaultControl,
    Shift: DefaultShift,
    Alt: DefaultAlt,
    Meta: DefaultMeta,
    Space: DefaultSpace,
    Character: DefaultCharacter,
    Text: DefaultText,
  },
}) => {
  const [commandParts, setCommandParts] = useState<string[]>([]);
  const [commandPartsDisplay, setCommandPartsDisplay] = useState<JSX.Element[]>(
    []
  );
  const [firstWrite, setFirstWrite] = useState<boolean>(true);
  const [overWrite, setOverWrite] = useState<boolean>(false);
  const [overWriteCount, setOverWriteCount] = useState<number>(0);
  const [allowedKeys, setAllowedKeys] = useState<string[]>([]);
  const [focus, setFocus] = useState<boolean>(false);

  const initializeCommandParts = useCallback(() => {
    setOverWriteCount(0);
    setOverWrite(false);

    if (maxCommandNumber === undefined) {
      maxCommandNumber = 4;
    }

    if (shortcutCommand !== undefined) {
      setCommandParts(shortcutCommand);
    } else {
      setCommandParts([emptyCommand]);
    }

    if (allowedKeysArg !== undefined) {
      setAllowedKeys(allowedKeysArg);
    } else {
      setAllowedKeys([
        "Control",
        "Shift",
        "Meta",
        "Alt",
        "Backspace",
        "Space",
        "NumKeys",
        "DigitKeys",
        "FKeys",
      ]);
    }
  }, [shortcutCommand, emptyCommand, allowedKeysArg]);

  useEffect(initializeCommandParts, [
    shortcutCommand,
    maxCommandNumber,
    emptyCommand,
  ]);

  const handleReturn = useCallback(
    (
      event: KeyboardEvent<HTMLDivElement> | React.FocusEvent<HTMLDivElement>,
      saveOrCancel: string
    ) => {
      const { target } = event;
      if (target instanceof HTMLInputElement) {
        target.blur();
      }
      setFirstWrite(true);

      switch (saveOrCancel) {
        case "save": {
          // eslint-disable-next-line no-console
          console.log(`id: ${id} commandParts: ${commandParts}`);
          if (isCommandPartsEmpty(commandParts, emptyCommand)) {
            handleShortcutPicker(id, undefined);
          } else {
            handleShortcutPicker(id, commandParts);
          }
          break;
        }
        case "cancel": {
          handleShortcutPicker(id, shortcutCommand);
          break;
        }
        default: {
          break;
        }
      }
    },
    [commandParts, id, shortcutCommand]
  );

  useEffect(() => {
    setCommandPartsDisplay(
      commandParts.map((cmd) => {
        const keyDisplayValuesArr = Object.keys(keyDisplayValues);

        for (let i = 0; i < keyDisplayValuesArr.length; i += 1) {
          if (cmd === keyDisplayValuesArr[i]) {
            const Component = keyDisplayValues[keyDisplayValuesArr[i]];
            return <Component />;
          }
        }

        if (cmd.length === 1) {
          const Component = keyDisplayValues.Character;
          return <Component character={cmd.toLocaleUpperCase()} />;
        }

        // comment

        const Component = keyDisplayValues.Text;
        return <Component text={cmd} />;
      })
    );
  }, [commandParts]);

  const handleKeyDown = useCallback(
    (e: KeyboardEvent<HTMLDivElement>) => {
      e.preventDefault();

      if (e.repeat) {
        return;
      }

      setOverWriteCount(overWriteCount + 1);

      if (e.key === " ") {
        e.key = "Space";
      }

      if (overWriteCount === 0) {
        setOverWrite(true);
      } else {
        setOverWrite(false);
      }

      if (e.key === "Enter") {
        handleReturn(e, "save");
        return;
      }
      if (e.key === "Escape") {
        initializeCommandParts();
        handleReturn(e, "cancel");
        return;
      }
      if (e.key === "Delete") {
        setCommandParts([emptyCommand]);
        return;
      }

      if (commandParts.length < maxCommandNumber || firstWrite || overWrite) {
        let CommandPartsTmp = [...commandParts];

        if (
          isCommandPartsEmpty(commandParts, emptyCommand) ||
          firstWrite ||
          overWrite
        ) {
          CommandPartsTmp = [];
          setFirstWrite(false);
          setOverWrite(false);
        }

        if (allowedKeys.includes(e.key)) {
          if (!CommandPartsTmp.includes(e.key)) {
            setCommandParts([...CommandPartsTmp, e.key]);
          }
        } else {
          let pressedKey: string = e.code;

          if (
            (pressedKey.includes("Digit") &&
              allowedKeys.includes("DigitKeys")) ||
            (pressedKey.includes("Numpad") &&
              allowedKeys.includes("NumKeys")) ||
            (pressedKey.includes("F") && allowedKeys.includes("FKeys")) ||
            pressedKey.includes("Key")
          ) {
            if (pressedKey[0] !== "F" && pressedKey.length !== 2) {
              pressedKey = pressedKey.slice(-1).toLowerCase();
            }

            if (!CommandPartsTmp.includes(pressedKey)) {
              setCommandParts([...CommandPartsTmp, pressedKey]);
            }
          }
        }
      }
    },
    [
      overWriteCount,
      emptyCommand,
      firstWrite,
      overWrite,
      commandParts,
      allowedKeys,
    ]
  );

  const handleFocus = useCallback(() => setFocus(true), []);

  const handleBlur = useCallback(
    (e: React.FocusEvent<HTMLDivElement>) => {
      handleReturn(e, "save");
      setFocus(false);
    },
    [focus, commandParts]
  );

  const handleKeyUp = useCallback(() => {
    setOverWriteCount(overWriteCount - 1);

    if (overWriteCount === 1) {
      setOverWrite(true);
    } else {
      setOverWrite(false);
    }
  }, [overWriteCount]);

  return (
    // eslint-disable-next-line jsx-a11y/no-static-element-interactions
    <div
      tabIndex={1}
      onKeyDown={handleKeyDown}
      onKeyUp={handleKeyUp}
      onFocus={handleFocus}
      onBlur={handleBlur}
      className="shortcutContainer"
    >
      {commandPartsDisplay.map((element, index) => (
        // eslint-disable-next-line react/no-array-index-key
        <div key={index}>{element}</div>
      ))}
    </div>
  );
};

export default ShortcutPicker;
